// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import '@openzeppelin/contracts/security/ReentrancyGuard.sol';

interface IWETH9 is IERC20 {
  function deposit() external payable;
  function withdraw(uint256 wad) external;
}

contract Vault is ReentrancyGuard{

    event EthReceived(uint256 _amount);
    event EthSent(uint256 _amount);
    event EthWrapped(uint256 _amount);
    event EthUnwrapped(uint256 _amount);

    address immutable private _weth9Token;

    struct TokenInfo{
        uint256 ethBalance;
        mapping(address => uint256) erc20Balances;
    }

    /*Map containing user balances*/
    mapping(address => TokenInfo) private userBalanceTable;
    
    constructor(address _weth9){
        _weth9Token = payable(_weth9);
    }

    receive() external payable {
        emit EthReceived(msg.value);
    }

    /*  User can deposit eth and erc20 token using this function. 
        If the user is depositing erc20 token, msg.value must be zero.
    */
    function deposit(address _tokenAddress, uint256 _amount) external payable{
        if (msg.value > 0) {
            userBalanceTable[msg.sender].ethBalance += msg.value;
        }
        else{
            require(_tokenAddress != address(0x0), "Token address cannot be zero!");
            require(_amount != 0, "Cannot deposit zero amount in vault");

            // Check allowance
            require(IERC20(_tokenAddress).allowance(msg.sender, address(this)) >= _amount, "Allowance not enough");

            userBalanceTable[msg.sender].erc20Balances[_tokenAddress] += _amount;
            IERC20(_tokenAddress).transferFrom(msg.sender, address(this), _amount);
        }
    }

    /*Wrap eth to weth. It does not send weth to the user but rather updates user weth balance in the contract.*/
    function wrap(uint256 _amount) external nonReentrant{
        require(userBalanceTable[msg.sender].ethBalance >= _amount, "Insufficient user eth deposit");
        IWETH9(_weth9Token).deposit{value : _amount}();
        userBalanceTable[msg.sender].ethBalance -= _amount;
        userBalanceTable[msg.sender].erc20Balances[_weth9Token] += _amount;
        emit EthWrapped(_amount);
    }

    /*Unwrap weth to eth. It does not send eth to the user but rather updates user eth balance in the contract.*/
    function unwrap(uint256 _amount) external nonReentrant{
        require(userBalanceTable[msg.sender].erc20Balances[_weth9Token] >= _amount, "Insufficient token balance");
        IWETH9(_weth9Token).withdraw(_amount);
        userBalanceTable[msg.sender].ethBalance += _amount;
        userBalanceTable[msg.sender].erc20Balances[_weth9Token] -= _amount;
        emit EthUnwrapped(_amount);
    }

    /*If _tokenAddress is 0x0, eth is withdrawn */
    function withdraw(address _tokenAddress, uint256 _amount) external nonReentrant{
        require(_amount != 0, "Deposit amount must be greater than 0");

        if(_tokenAddress == address(0x0)){
            require(userBalanceTable[msg.sender].ethBalance >= _amount, "Insufficient user eth deposit");
            payable(msg.sender).transfer(_amount);
            emit EthSent(_amount);
            userBalanceTable[msg.sender].ethBalance -= _amount;
        }
        else{
            require(userBalanceTable[msg.sender].erc20Balances[_tokenAddress] >= _amount, "Insufficient token balance");
            ERC20(_tokenAddress).transfer(msg.sender, _amount);
            userBalanceTable[msg.sender].erc20Balances[_tokenAddress] -= _amount;
        }
    }

    function getEthBalance() external view returns(uint256){
        return userBalanceTable[msg.sender].ethBalance;
    }

    function getTokenBalance(address _tokenAddress) external view returns(uint256){
        return userBalanceTable[msg.sender].erc20Balances[_tokenAddress];
    }
}
